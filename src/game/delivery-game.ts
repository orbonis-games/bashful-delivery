import { Collision } from "libs/core-game/src/game/collision";
import { Game } from "libs/core-game/src/game/game";
import { SFXData } from "libs/core-game/src/game/sfx";
import { Graphics, Point, Rectangle } from "pixi.js";
import { Box } from "./box";
import { DropLocations } from "./drop-locations";
import { DropPoint } from "./drop-point";
import { Instructions } from "./instructions";
import { Protag } from "./protag";
import { Town } from "./town";

export class DeliveryGame extends Game {
    private town?: Town;
    private protag?: Protag;
    private box?: Box;
    private dropPoint?: DropPoint;
    private instructions?: Instructions;

    public async init(canvas: HTMLCanvasElement, sounds?: SFXData[]): Promise<void> {
        await super.init(canvas, sounds);
        this.reset();
    }

    protected async initRenderables(): Promise<void> {
        if (this.app) {
            this.protag = new Protag("protag", this.app);
            this.dropPoint = new DropPoint("drop", this.app, new Rectangle(0, 0, 50, 50));
            this.town = new Town("town", this.app);
            this.box = new Box("box", this.app, new Rectangle(780, 330, 20, 20));
            this.instructions = new Instructions("instructions", this.app);

            this.box.onInteract = () => {
                const position: Point = this.getNextDropPosition();
                this.dropPoint?.move(position);
                this.dropPoint?.setVisible(true);
                this.instructions?.hidePopup().then(() => {
                    this.instructions?.showPopup(position.x + 25, position.y, "Drop the box\nhere!");
                });
            };

            this.dropPoint.onInteract = () => {
                this.box?.setVisible(true);
                this.instructions?.hidePopup().then(() => {
                    this.instructions?.showPopup(790, 320, "Pick up the\nbox!");
                });
            };

            const shadow: Graphics = new Graphics();
            this.app.stage.addChild(shadow);
            shadow.beginFill(0xFFFFFF, 1);
            shadow.drawRect(0, 0, 1000, 1000);
            shadow.endFill();
            shadow.mask = this.protag.getVisionMask();
            
            this.town.setMask(this.protag.getVisionMask());

            await super.initRenderables(
                this.dropPoint,
                this.box,
                this.protag,
                this.town,
                this.instructions
            );

            this.box.setMask(this.protag.getVisionMask());
        }
    }

    protected prerender(delta: number): void {
        const collision: Collision = this.getCollision();
        const position: Point | undefined = this.protag?.getPosition();
        if (position) {
            this.protag?.updateVisionPoints(collision.raycast(position));
        }
    }

    private reset(): void {
        this.box?.setVisible(true);
        this.dropPoint?.setVisible(false);
        this.instructions?.showPopup(790, 320, "Pick up the\nbox!");
    }

    private getNextDropPosition(): Point {
        const index: number = Math.floor(Math.random() * DropLocations.length);
        return DropLocations[index].clone();
    }
}
