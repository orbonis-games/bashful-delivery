import { Container, Graphics } from "pixi.js";
import { Interactive } from "./interactive";

export class Box extends Interactive {
    public setMask(mask: Container): void {
        if (this.container) {
            this.container.mask = mask;
        }
    }

    protected createBox(): Graphics {
        const graphics: Graphics = new Graphics();
        
        graphics.lineStyle(0, 0, 0);
        graphics.beginFill(0x000000, 0.2);
        graphics.drawPolygon([
            0, this.size.height,
            5, this.size.height + 5,
            this.size.width + 5, this.size.height + 5,
            this.size.width + 5, 5,
            this.size.width, 0
        ]);
        graphics.endFill();

        graphics.lineStyle(2, 0x222222);
        graphics.beginFill(0xCCCCCC);
        graphics.drawRect(0, 0, this.size.width, this.size.height);
        graphics.endFill();

        return graphics;
    }
}
