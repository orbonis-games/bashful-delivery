import { Renderable } from "libs/core-game/src/game/renderable";
import { BLEND_MODES, Circle, Container, DisplayObject, Graphics, Rectangle, Sprite, Texture } from "pixi.js";
import Intersects from "intersects";
import { CollidableShape } from "libs/core-game/src/game/collision";

export interface Structure {
    bounds: Rectangle | Circle;
    graphics: DisplayObject;
}

export class Town extends Renderable {
    private container?: Container;
    private structures: Structure[] = [];

    private waterTexture?: Texture;
    private mask?: Container;

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("water", "./assets/water.png");
            this.app.loader.load(() => {
                this.container = new Container();
                this.app.stage.addChild(this.container);

                this.waterTexture = this.app.loader.resources["water"].texture;
        
                this.structures.push(this.createHouse(new Rectangle(0, 0, 200, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(210, 0, 200, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(420, 0, 300, 100), 10));
                
                this.structures.push(this.createHouse(new Rectangle(550, 370, 100, 200), 10));
        
                this.structures.push(this.createHouse(new Rectangle(730, 0, 100, 300), 10));
                this.structures.push(this.createHouse(new Rectangle(730, 310, 100, 260), 10));
                
                this.structures.push(this.createHouse(new Rectangle(0, 110, 100, 260), 10));
                this.structures.push(this.createHouse(new Rectangle(0, 380, 100, 300), 10));
                
                this.structures.push(this.createHouse(new Rectangle(110, 170, 250, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(170, 340, 80, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(260, 280, 100, 160), 10));
                this.structures.push(this.createHouse(new Rectangle(170, 500, 190, 70), 10));
        
                this.structures.push(this.createHouse(new Rectangle(110, 580, 300, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(420, 580, 200, 100), 10));
                this.structures.push(this.createHouse(new Rectangle(630, 580, 200, 100), 10));
                
                this.structures.push(this.createHouse(new Rectangle(550, 110, 100, 200), 10));
        
                this.structures.push(this.createFountain(new Circle(450, 340, 40), 10));
        
                this.container.addChild(...this.structures.map((x) => x.graphics));

                this.container.x = 100;
                this.container.y = 200;

                this.blockVision = true;
    
                resolve();
            });
        });
    }

    public render(delta: number): void {
        // Do nothing
    }

    public setMask(mask: Container): void {
        this.mask = mask;
    }

    public checkCollision(subject?: Circle): boolean {
        if (this.container && subject) {
            for (const structure of this.structures) {
                const bounds: Rectangle | Circle = structure.bounds.clone();
                bounds.x += this.container.x;
                bounds.y += this.container.y;

                let collides: boolean = false;
                
                if (bounds instanceof Rectangle) {
                    collides = Intersects.boxCircle(
                        bounds.x, bounds.y, bounds.width, bounds.height,
                        subject.x, subject.y, subject.radius
                    );
                } else if (bounds instanceof Circle) {
                    collides = Intersects.circleCircle(
                        subject.x, subject.y, subject.radius,
                        bounds.x, bounds.y, bounds.radius
                    );
                }
                
                if (collides) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }

    public getColliders(): CollidableShape[] {
        if (this.container) {
            return this.structures.map((x) => {
                const bounds: CollidableShape = x.bounds.clone();
                bounds.x += this.container!.x;
                bounds.y += this.container!.y;
                return bounds;
            });
        } else {
            return [];
        }
    }

    private createHouse(bounds: Rectangle, shadowDistance: number): Structure {
        const graphics: Graphics = new Graphics();

        graphics.lineStyle(0, 0, 0);
        graphics.beginFill(0x00000, 0.2);
        graphics.drawPolygon([
            0, bounds.height,
            shadowDistance + 5, bounds.height + shadowDistance,
            bounds.width + shadowDistance + 5, bounds.height + shadowDistance,
            bounds.width + shadowDistance + 5, shadowDistance,
            bounds.width, 0
        ]);
        graphics.endFill();

        graphics.lineStyle(2, 0x222222);
        graphics.beginFill(0xFFFFFF);
        graphics.drawRect(0, 0, bounds.width, bounds.height);
        graphics.endFill();

        if (bounds.width > bounds.height) {
            graphics.lineStyle(0, 0, 0);
            graphics.beginFill(0xCCCCCC);
            graphics.drawPolygon([
                bounds.width - 2, 1,
                bounds.width - (bounds.height / 2), bounds.height * 0.5,
                bounds.width - 2, bounds.height - 2
            ]);
            graphics.endFill();
            graphics.beginFill(0xDDDDDD);
            graphics.drawPolygon([
                1, bounds.height - 2,
                (bounds.height / 2), bounds.height / 2,
                bounds.width - (bounds.height / 2), bounds.height / 2,
                bounds.width - 2, bounds.height - 2
            ]);
            graphics.endFill();
            graphics.beginFill(0xF0F0F0);
            graphics.drawPolygon([
                1, 1,
                (bounds.height / 2), bounds.height / 2,
                1, bounds.height - 2
            ]);
            graphics.endFill();
        } else {
            graphics.lineStyle(0, 0, 0);
            graphics.beginFill(0xCCCCCC);
            graphics.drawPolygon([
                bounds.width - 2, 1,
                bounds.width * 0.5, (bounds.width / 2),
                bounds.width * 0.5, bounds.height - (bounds.width / 2),
                bounds.width - 2, bounds.height - 2
            ]);
            graphics.endFill();
            graphics.beginFill(0xDDDDDD);
            graphics.drawPolygon([
                1, bounds.height - 2,
                bounds.width / 2, bounds.height - (bounds.width / 2),
                bounds.width - 2, bounds.height - 2
            ]);
            graphics.endFill();
            graphics.beginFill(0xF0F0F0);
            graphics.drawPolygon([
                1, 1,
                bounds.width / 2, (bounds.width / 2),
                bounds.width / 2, bounds.height - (bounds.width / 2),
                1, bounds.height - 2
            ]);
            graphics.endFill();
        }

        graphics.x = bounds.x;
        graphics.y = bounds.y;
        graphics.cacheAsBitmap = true;

        return { bounds, graphics };
    }

    private createFountain(bounds: Circle, shadowDistance: number): Structure {
        const graphics: Graphics = new Graphics();
        
        graphics.lineStyle(0, 0, 0);
        graphics.beginFill(0x000000, 0.2);
        graphics.drawEllipse(
            shadowDistance,
            shadowDistance,
            bounds.radius,
            bounds.radius
        );
        graphics.endFill();

        graphics.lineStyle(2, 0x222222);
        graphics.beginFill(0xFFFFFF);
        graphics.drawEllipse(
            0,
            0,
            bounds.radius,
            bounds.radius
        );
        graphics.endFill();

        graphics.lineStyle(0, 0, 0);
        graphics.beginFill(0xDDDDDD);
        graphics.arc(0, 0, bounds.radius, -(Math.PI / 4), Math.PI * 0.75);
        graphics.endFill();
        
        graphics.lineStyle(2, 0x222222);
        graphics.beginFill(0xFFFFFF);
        graphics.drawEllipse(
            0,
            0,
            (bounds.radius) - shadowDistance,
            (bounds.radius) - shadowDistance
        );
        graphics.endFill();
        graphics.cacheAsBitmap = true;

        const waterHole: Graphics = new Graphics();
        waterHole.beginFill(0xDDDDFF);
        waterHole.drawEllipse(
            0,
            0,
            (bounds.radius) - (shadowDistance * 2),
            (bounds.radius) - (shadowDistance * 2)
        );
        waterHole.endFill();
        waterHole.cacheAsBitmap = true;

        const water: Sprite = new Sprite(this.waterTexture);
        water.blendMode = BLEND_MODES.MULTIPLY;
        water.width = (bounds.radius * 2) - (shadowDistance * 4);
        water.height = (bounds.radius * 2) - (shadowDistance * 4);
        water.x = -(bounds.radius / 2);
        water.y = -(bounds.radius / 2);
        water.alpha = 0.5;

        const container: Container = new Container();
        container.x = bounds.x;
        container.y = bounds.y;
        container.addChild(graphics);
        container.addChild(waterHole);
        container.addChild(water);
        container.mask = this.mask ?? null;
        return { graphics: container, bounds };        
    }
}