import { Renderable } from "libs/core-game/src/game/renderable";
import { LoadFont } from "libs/core-game/src/utils/load-font";
import { Application, Container, Graphics, Rectangle } from "pixi.js";

export class Interactive extends Renderable {
    public onInteract?: () => void;

    protected container?: Container;
    protected box?: Container;

    protected visible: boolean = false;

    constructor(id: string, app: Application, protected size: Rectangle) {
        super(id, app);
    }

    public init(): Promise<void> {
        return new Promise((resolve) => {
            LoadFont("Indie Flower").then(() => {
                this.container = new Container();
                this.app.stage.addChild(this.container);
        
                this.box = this.createBox();
                this.box.visible = this.visible;
                this.container.addChild(this.box);
        
                this.container.x = this.size.x;
                this.container.y = this.size.y;
                resolve();
            });
        });
    }

    public render(delta: number): void {
        if (this.box) {
            if (this.colliding === "protag") {
                this.setVisible(false);
                if (this.onInteract) {
                    this.onInteract();
                }
            }
        }
    }

    public getColliders(): Rectangle[] {
        if (this.container && this.box && this.visible) {
            return [ new Rectangle(this.container.x, this.container.y, this.size.width, this.size.height) ];
        } else {
            return [];
        }
    }

    public setVisible(value: boolean): void {
        if (this.box) {
            this.visible = value;
            this.box.visible = value;
        }
    }

    protected createBox(): Graphics {
        return new Graphics();
    }
}
