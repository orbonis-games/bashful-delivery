import { Graphics, Point, Rectangle } from "pixi.js";
import { Interactive } from "./interactive";

export class DropPoint extends Interactive {
    public getColliders(): Rectangle[] {
        if (this.container && this.box && this.visible) {
            return [
                new Rectangle(
                    this.container.x + 10,
                    this.container.y + 10,
                    this.size.width - 20, 
                    this.size.height - 20
                )
            ];
        } else {
            return [];
        }
    }

    public move(position: Point): void {
        if (this.container) {
            this.container.x = position.x;
            this.container.y = position.y;
        }
    }

    protected createBox(): Graphics {
        const graphics: Graphics = new Graphics();
        
        graphics.lineStyle(2, 0xAA0000);
        graphics.beginFill(0xAA0000, 0.2);
        graphics.drawRect(0, 0, this.size.width, this.size.height);
        graphics.endFill();

        return graphics;
    }
}