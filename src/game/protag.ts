import { Renderable } from "libs/core-game/src/game/renderable";
import { BLEND_MODES, Circle, Container, DisplayObject, Graphics, Point, Sprite, Texture } from "pixi.js";
import intersects from "intersects";
import { Line, Ray } from "libs/core-game/src/utils/ray";

export class Protag extends Renderable {
    private readonly speed: number = 50;

    private container?: Container;
    private box?: Container;
    private vision?: Graphics;
    private moveDelta: Point = new Point(0, 0);
    private previousPosition: Point = new Point(0, 0);
    private bounds: Circle = new Circle(0, 0, 0);
    private steps: number = 0;
    private shirtTexture?: Texture;
    private speedMod: number = 1;
    private visionPoints: Point[] = [];

    public init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.loader.reset();
            this.app.loader.add("flowers", "./assets/flowers.png");
            this.app.loader.load(() => {
                this.shirtTexture = this.app.loader.resources["flowers"].texture;

                this.container = new Container();
                this.app.stage.addChild(this.container);
        
                this.container.addChild(this.createPerson());
        
                this.container.x = 790;
                this.container.y = 745;
                this.previousPosition = this.container.position.clone();
                this.steps = 0;
        
                this.box = this.createBox();
                this.container.addChild(this.box);
                this.box.y = -5;
                this.box.visible = false;
                
                this.bounds.radius = this.container.width / 2;
                this.bounds.x = this.container.x;
                this.bounds.y = this.container.y;

                this.setupControls();

                resolve();
            });
        });
    }

    public render(delta: number): void {
        if (this.container && this.box) {
            if (this.box.visible) {
                this.speedMod = 1;
            }

            switch (this.colliding) {
                case "box":
                    this.toggleBox(true);
                    break;
                case "drop":
                    this.toggleBox(false);
                    break;
                case "town":
                    this.container.x = this.previousPosition.x;
                    this.container.y = this.previousPosition.y;
                    break;
                default:
                    this.previousPosition = this.container.position.clone();
                    this.container.x += delta * this.moveDelta.x * (this.speed * this.speedMod);
                    this.container.y += delta * this.moveDelta.y * (this.speed * this.speedMod);
    
                    const diff: Point = new Point(
                        this.container.x - this.previousPosition.x,
                        this.container.y - this.previousPosition.y
                    );
    
                    if (diff.x !== 0 || diff.y !== 0) {
                        const rotation: number = Math.atan2(diff.y, diff.x);
                        this.container.rotation = rotation + Math.PI * 0.5;
    
                        this.steps += delta * (this.speed * this.speedMod) / 4;
    
                        const scale: number = (((Math.sin(this.steps) + 1) / 2) * 0.1);
                        this.container.scale.set(1 + scale);
                        this.box.scale.set(1 - scale);
                    }
                    break;
            }

            this.bounds.x = this.container.x;
            this.bounds.y = this.container.y;

            this.redrawVision();
        }
    }

    public getColliders(): Circle[] {
        return [this.bounds];
    }

    public isActiveCollider(): boolean {
        return true;
    }

    public toggleBox(box: boolean): void {
        if (this.box) {
            this.box.visible = box;
        }
    }

    public getPosition(): Point | undefined {
        return this.container?.position.clone();
    }

    public updateVisionPoints(points: Point[]): void {
        this.visionPoints = points;
    }

    public getVisionMask(): Graphics {
        if (!this.vision) {
            this.redrawVision();
        }
        return this.vision!;
    }

    private createPerson(): Container {
        const body: Graphics = new Graphics();
        
        body.lineStyle(2, 0x222222);

        body.beginFill(0xFFFFFF);
        body.drawCircle(0, 0, 20);
        body.endFill();

        body.cacheAsBitmap = true;

        const head: Graphics = new Graphics();
        
        head.lineStyle(2, 0x222222);

        head.beginFill(0xFFFFFF);
        head.drawCircle(0, 5, 7);
        head.endFill();
        
        head.beginFill(0xFFFFFF);
        head.drawCircle(-15, -10, 5);
        head.endFill();
        
        head.beginFill(0xFFFFFF);
        head.drawCircle(15, -10, 5);
        head.endFill();

        head.cacheAsBitmap = true;

        const shirt: Sprite = new Sprite(this.shirtTexture);
        shirt.blendMode = BLEND_MODES.MULTIPLY;
        shirt.width = 40;
        shirt.height = 40;
        shirt.x = -20;
        shirt.y = -20;

        const container: Container = new Container();
        container.addChild(body, shirt, head);
        return container;
    }

    private createBox(): Container {
        const graphics: Graphics = new Graphics();
        graphics.lineStyle(2, 0x222222);
        graphics.beginFill(0xCCCCCC);
        graphics.drawRect(-10, -20, 20, 20);
        graphics.endFill();

        const container: Container = new Container();
        container.addChild(graphics);
        return container;
    }

    private setupControls(): void {
        window.addEventListener("keydown", (e) => {
            switch (e.key) {
                case "ArrowUp":
                    this.moveDelta.y = -1;
                    break;
                case "ArrowDown":
                    this.moveDelta.y = 1;
                    break;
                case "ArrowLeft":
                    this.moveDelta.x = -1;
                    break;
                case "ArrowRight":
                    this.moveDelta.x = 1;
                    break;
                case "Shift":
                    this.speedMod = 2;
                    break;
            }
        });

        window.addEventListener("keyup", (e) => {
            switch (e.key) {
                case "ArrowUp":
                    if (this.moveDelta.y < 0) {
                        this.moveDelta.y = 0;
                    }
                    break;
                case "ArrowDown":
                    if (this.moveDelta.y > 0) {
                        this.moveDelta.y = 0;
                    }
                    break;
                case "ArrowLeft":
                    if (this.moveDelta.x < 0) {
                        this.moveDelta.x = 0;
                    }
                    break;
                case "ArrowRight":
                    if (this.moveDelta.x > 0) {
                        this.moveDelta.x = 0;
                    }
                    break;
                case "Shift":
                    this.speedMod = 1;
                    break;
            }
        });
    }

    private redrawVision(): void {
        if (!this.vision) {
            this.vision = new Graphics();
            this.app.stage.addChildAt(this.vision, 0);
        }

        if (this.container) {
            this.vision.clear();
            
            const points: number[] = this.visionPoints.reduce<number[]>((prev, curr) => [...prev, curr.x, curr.y], []);
            this.vision.beginFill(0x000000, 0.5);
            this.vision.drawPolygon(points);
            this.vision.endFill();
        }
    }
}