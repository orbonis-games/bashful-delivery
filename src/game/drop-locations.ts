import { Point } from "pixi.js";

export const DropLocations: Point[] = [
    new Point(300, 480),
    new Point(210, 310),
    new Point(210, 720),
    new Point(765, 720)
];
