import { Easing, Tween } from "@tweenjs/tween.js";
import { CollidableShape } from "libs/core-game/src/game/collision";
import { Renderable } from "libs/core-game/src/game/renderable";
import { Container, Graphics, Rectangle, Text, TextStyle } from "pixi.js";

export class Instructions extends Renderable {
    private container?: Container;
    private popup?: Container;
    private bobTween?: Tween<any>;
    private visible: boolean = false;

    public init(): Promise<void | void[]> {
        this.container = new Container();
        this.app.stage.addChild(this.container);
        return new Promise((resolve) => resolve());
    }

    public render(delta: number): void {
        if (this.colliding === "protag") {
            this.hidePopup();
        }
    }

    public getColliders(): CollidableShape[] {
        if (this.popup && this.visible) {
            return [
                new Rectangle(
                    this.popup.x - 60,
                    this.popup.y - 60,
                    120,
                    50
                )
            ];
        } else {
            return [];
        }
    }

    public showPopup(x: number, y: number, text: string): Promise<void> {
        return new Promise((resolve) => {
            if (!this.popup) {
                const background: Graphics = new Graphics();
                background.lineStyle(2, 0x222222);
                background.beginFill(0xFFFFFF);
                background.drawRoundedRect(-60, -60, 120, 50, 10);
                background.moveTo(-10, -11);
                background.lineTo(0, 0);
                background.lineTo(10, -11);
                background.endFill();
    
                const label: Text = new Text(text);
                label.style = new TextStyle({
                    fontSize: 18,
                    align: "center",
                    fontFamily: "Indie Flower",
                    fontWeight: "bold"
                });
                label.anchor.set(0.5, 0);
                label.x = 0;
                label.y = -54;
    
                this.popup = new Container();
                this.popup.addChild(background, label);
                this.popup.x = x;
                this.popup.y = y;
                this.popup.scale.set(0);
                this.container?.addChild(this.popup);
    
                this.bobTween = new Tween(this.popup!)
                    .to({ y: y - 10 }, 1000)
                    .yoyo(true)
                    .repeat(Number.MAX_SAFE_INTEGER)
                    .easing(Easing.Quadratic.InOut)
                    .start();
            }
            
            this.visible = true;
            new Tween(this.popup.scale)
                .to({ x: 1, y: 1 }, 250)
                .easing(Easing.Back.Out)
                .onComplete(() => resolve())
                .start();
        });
    }

    public hidePopup(): Promise<void> {
        return new Promise((resolve) => {
            if (this.popup) {
                this.visible = false;
                new Tween(this.popup.scale)
                    .to({ x: 0, y: 0 }, 250)
                    .easing(Easing.Back.In)
                    .onComplete(() => {
                        this.bobTween?.stop();
                        this.bobTween = undefined;
                        this.popup?.destroy();
                        this.popup = undefined;
                        resolve();
                    })
                    .start();
            } else {
                resolve();
            }
        });
    }
}